export declare class Supplier {
    id: number;
    drg_name: string;
    supplier_id: number;
    name: string;
    street: string;
    city: string;
    state: string;
    zip_code: string;
    hospital_referral_region_description: string;
    total_discharges: number;
    avg_covered_charges: number;
    avg_total_payments: number;
    avg_medicare_payments: number;
}
