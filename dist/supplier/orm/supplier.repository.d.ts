import { Supplier } from './supplier.entity';
import { Repository } from 'typeorm';
export declare class SupplierRepository extends Repository<Supplier> {
}
