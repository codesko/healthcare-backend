"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const class_transformer_1 = require("class-transformer");
const dollarFormatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
});
let Supplier = class Supplier {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    class_transformer_1.Exclude(),
    __metadata("design:type", Number)
], Supplier.prototype, "id", void 0);
__decorate([
    class_transformer_1.Transform(value => value.substr(0, value.indexOf(' '))),
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Supplier.prototype, "drg_name", void 0);
__decorate([
    typeorm_1.Column('int'),
    class_transformer_1.Exclude(),
    __metadata("design:type", Number)
], Supplier.prototype, "supplier_id", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Supplier.prototype, "name", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Supplier.prototype, "street", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Supplier.prototype, "city", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Supplier.prototype, "state", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Supplier.prototype, "zip_code", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Supplier.prototype, "hospital_referral_region_description", void 0);
__decorate([
    typeorm_1.Column('int'),
    __metadata("design:type", Number)
], Supplier.prototype, "total_discharges", void 0);
__decorate([
    class_transformer_1.Transform(value => dollarFormatter.format(value)),
    typeorm_1.Column('decimal', { precision: 10, scale: 2 }),
    __metadata("design:type", Number)
], Supplier.prototype, "avg_covered_charges", void 0);
__decorate([
    class_transformer_1.Transform(value => dollarFormatter.format(value)),
    typeorm_1.Column('decimal', { precision: 10, scale: 2 }),
    __metadata("design:type", Number)
], Supplier.prototype, "avg_total_payments", void 0);
__decorate([
    class_transformer_1.Transform(value => dollarFormatter.format(value)),
    typeorm_1.Column('decimal', { precision: 10, scale: 2 }),
    __metadata("design:type", Number)
], Supplier.prototype, "avg_medicare_payments", void 0);
Supplier = __decorate([
    typeorm_1.Entity()
], Supplier);
exports.Supplier = Supplier;
//# sourceMappingURL=supplier.entity.js.map