import { Request } from 'express';
import { SupplierQuery } from './interfaces/supplier.interface';
import { SupplierService } from './supplier.service';
export declare class SupplierController {
    private readonly supplierService;
    constructor(supplierService: SupplierService);
    search(query: SupplierQuery, req: Request): Promise<import("nestjs-typeorm-paginate").Pagination<import("./orm/supplier.entity").Supplier>>;
}
