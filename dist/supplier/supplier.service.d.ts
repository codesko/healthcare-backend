import { Supplier } from './orm/supplier.entity';
import { SupplierQuery } from './interfaces/supplier.interface';
import { SupplierRepository } from './orm/supplier.repository';
import { Pagination } from 'nestjs-typeorm-paginate';
export declare class SupplierService {
    private readonly repository;
    constructor(repository: SupplierRepository);
    paginate(query: SupplierQuery): Promise<Pagination<Supplier>>;
}
