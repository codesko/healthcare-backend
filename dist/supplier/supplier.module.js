"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const supplier_controller_1 = require("./supplier.controller");
const supplier_service_1 = require("./supplier.service");
const typeorm_1 = require("@nestjs/typeorm");
const supplier_repository_1 = require("./orm/supplier.repository");
let SupplierModule = class SupplierModule {
};
SupplierModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([supplier_repository_1.SupplierRepository])],
        controllers: [supplier_controller_1.SupplierController],
        providers: [supplier_service_1.SupplierService],
    })
], SupplierModule);
exports.SupplierModule = SupplierModule;
//# sourceMappingURL=supplier.module.js.map