"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const supplier_repository_1 = require("./orm/supplier.repository");
const nestjs_typeorm_paginate_1 = require("nestjs-typeorm-paginate");
const typeorm_1 = require("typeorm");
let SupplierService = class SupplierService {
    constructor(repository) {
        this.repository = repository;
    }
    paginate(query) {
        return __awaiter(this, void 0, void 0, function* () {
            const between = name => min => max => {
                const both = min !== undefined && max !== undefined;
                const onlyMin = min !== undefined && max === undefined;
                const onlyMax = min === undefined && max !== undefined;
                switch (true) {
                    case both:
                        return {
                            [name]: typeorm_1.Between(min, max),
                        };
                    case onlyMin:
                        return {
                            [name]: typeorm_1.MoreThanOrEqual(min),
                        };
                    case onlyMax:
                        return {
                            [name]: typeorm_1.LessThanOrEqual(max),
                        };
                    default:
                        return {};
                }
            };
            const exact = name => value => {
                if (value !== undefined) {
                    return {
                        [name]: value,
                    };
                }
                return {};
            };
            const like = name => value => {
                if (value !== undefined) {
                    return {
                        [name]: typeorm_1.Like(`%${value}%`),
                    };
                }
                return {};
            };
            const where = Object.assign({}, like('name')(query.name), exact('state')(query.state), between('total_discharges')(query.min_discharges)(query.max_discharges), between('avg_covered_charges')(query.min_average_covered_charges)(query.max_average_medicare_payments), between('avg_medicare_payments')(query.min_average_medicare_payments)(query.max_average_medicare_payments));
            return yield nestjs_typeorm_paginate_1.paginate(this.repository, query, { where });
        });
    }
};
SupplierService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [supplier_repository_1.SupplierRepository])
], SupplierService);
exports.SupplierService = SupplierService;
//# sourceMappingURL=supplier.service.js.map