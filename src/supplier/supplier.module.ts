import { Module } from '@nestjs/common';
import { SupplierController } from './supplier.controller';
import { SupplierService } from './supplier.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SupplierRepository } from './orm/supplier.repository';

@Module({
    imports: [TypeOrmModule.forFeature([SupplierRepository])],
    controllers: [SupplierController],
    providers: [SupplierService],
})
export class SupplierModule {}
