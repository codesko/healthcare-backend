import {
    Controller,
    Get,
    Query,
    UseInterceptors,
    ClassSerializerInterceptor,
    Req,
} from '@nestjs/common';
import { Request } from 'express';
import { SupplierQuery } from './interfaces/supplier.interface';
import { SupplierService } from './supplier.service';

@Controller()
export class SupplierController {
    constructor(private readonly supplierService: SupplierService) {}

    @UseInterceptors(ClassSerializerInterceptor)
    @Get('/providers')
    async search(@Query() query: SupplierQuery, @Req() req: Request) {
        query.limit = query.limit || 25;
        query.page = query.page || 1;
        query.route = req.protocol + '://' + req.get('host') + req.originalUrl;

        return await this.supplierService.paginate(query);
    }
}
