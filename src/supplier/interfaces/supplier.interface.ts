import { IPaginationOptions } from 'nestjs-typeorm-paginate';

export interface SupplierQuery extends IPaginationOptions {
    max_discharges?: number;
    min_discharges?: number;
    max_average_covered_charges?: number;
    min_average_covered_charges?: number;
    max_average_medicare_payments?: number;
    min_average_medicare_payments?: number;
    state?: string;
    name?: string;
}

export interface SupplierEntity {
    name: string;
    street: string;
    city: string;
    state: string;
    zip_code: string;
    hospital_referral_region_description: string;
    total_discharges: number;
    avg_covered_charges: number;
    avg_total_payments: number;
    avg_medicare_payments: number;
}
