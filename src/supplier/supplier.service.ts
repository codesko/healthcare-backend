import { Injectable } from '@nestjs/common';
import { Supplier } from './orm/supplier.entity';
import { SupplierQuery } from './interfaces/supplier.interface';
import { SupplierRepository } from './orm/supplier.repository';
import { paginate, Pagination } from 'nestjs-typeorm-paginate';
import { MoreThanOrEqual, LessThanOrEqual, Between, Like } from 'typeorm';

@Injectable()
export class SupplierService {
    constructor(private readonly repository: SupplierRepository) {}

    async paginate(query: SupplierQuery): Promise<Pagination<Supplier>> {
        const between = name => min => max => {
            const both = min !== undefined && max !== undefined;
            const onlyMin = min !== undefined && max === undefined;
            const onlyMax = min === undefined && max !== undefined;

            switch (true) {
                case both:
                    return {
                        [name]: Between(min, max),
                    };

                case onlyMin:
                    return {
                        [name]: MoreThanOrEqual(min),
                    };

                case onlyMax:
                    return {
                        [name]: LessThanOrEqual(max),
                    };

                default:
                    return {};
            }
        };

        const exact = name => value => {
            if (value !== undefined) {
                return {
                    [name]: value,
                };
            }

            return {};
        };

        const like = name => value => {
            if (value !== undefined) {
                return {
                    [name]: Like(`%${value}%`),
                };
            }

            return {};
        };

        const where = {
            ...like('name')(query.name),
            ...exact('state')(query.state),
            ...between('total_discharges')(query.min_discharges)(
                query.max_discharges
            ),
            ...between('avg_covered_charges')(
                query.min_average_covered_charges
            )(query.max_average_medicare_payments),
            ...between('avg_medicare_payments')(
                query.min_average_medicare_payments
            )(query.max_average_medicare_payments),
        };

        return await paginate<Supplier>(this.repository, query, { where });
    }
}
