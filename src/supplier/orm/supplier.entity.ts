import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { Transform, Exclude } from 'class-transformer';

const dollarFormatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
});
@Entity()
export class Supplier {
    @PrimaryGeneratedColumn()
    @Exclude()
    id: number;

    @Transform(value => value.substr(0, value.indexOf(' ')))
    @Column('text')
    drg_name: string;

    @Column('int')
    @Exclude()
    supplier_id: number;

    @Column('text')
    name: string;

    @Column('text')
    street: string;

    @Column('text')
    city: string;

    @Column('text')
    state: string;

    @Column('text')
    zip_code: string;

    @Column('text')
    hospital_referral_region_description: string;

    @Column('int')
    total_discharges: number;

    @Transform(value => dollarFormatter.format(value))
    @Column('decimal', { precision: 10, scale: 2 })
    avg_covered_charges: number;

    @Transform(value => dollarFormatter.format(value))
    @Column('decimal', { precision: 10, scale: 2 })
    avg_total_payments: number;

    @Transform(value => dollarFormatter.format(value))
    @Column('decimal', { precision: 10, scale: 2 })
    avg_medicare_payments: number;
}
