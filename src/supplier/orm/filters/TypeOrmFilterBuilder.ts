import { Between, MoreThanOrEqual, LessThanOrEqual, Like, Any } from 'typeorm';
import produce, { immerable } from 'immer';

export class TypeOrmFilterBuilder {
    private [immerable] = [];

    public between = name => min => max => {
        const both = min !== undefined && max !== undefined;
        const onlyMin = min !== undefined && max === undefined;
        const onlyMax = min === undefined && max !== undefined;

        switch (true) {
            case both:
                produce(this[immerable], (draft: any) => {
                    draft.push({
                        [name]: Between(min, max),
                    });
                });
                break;

            case onlyMin:
                produce(this[immerable], (draft: any) => {
                    draft.push({
                        [name]: MoreThanOrEqual(min),
                    });
                });
                break;

            case onlyMax:
                produce(this[immerable], (draft: any) => {
                    draft.push({
                        [name]: LessThanOrEqual(max),
                    });
                });
                break;

            default:
                break;
        }

        return this;
    };

    public exact = name => value => {
        if (value !== undefined) {
            produce(this[immerable], (draft: any) => {
                draft.push({
                    [name]: value,
                });
            });
            console.log(this[immerable]);
        }

        return this;
    };

    public like = name => value => {
        if (value !== undefined) {
            produce(this[immerable], (draft: any) => {
                draft.push({
                    [name]: Like(`%${value}%`),
                });
            });
        }

        return this;
    };

    public build = () => {
        return this[immerable];
    };
}
