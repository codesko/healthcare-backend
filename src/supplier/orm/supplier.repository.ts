import { Supplier } from './supplier.entity';
import { EntityRepository, Repository } from 'typeorm';
@EntityRepository(Supplier)
export class SupplierRepository extends Repository<Supplier> {}
