# Problems spotted

### The amount of data

For default, there are 163.065 records, so I decided to implement server-side pagination additionally. In result the time of loading is minimal.

### The amount of columns

Another problem is the number of columns and the responsive design of the table. It is always difficult to choose a proper solution for table showing. In this case, I decided to cut the DRG name and display the only ID, which is associated with a particular description, according to DSG definition. For mobiles I applied a trick to display table in a reverse manner, so is quite readable. For smaller devices (below 1090 width) table is scrollable horizontally, so we don't have to cut the data.

### DRG

Each record in the database is associated primarily with DRG, so when we looking for the providers they will be duplicated. I thought to do something like a table inside the table (nested tables), but I claimed finally, that providers duplication is not a big issue in this particular situation, though it is important to remember to show DRG column first.

# Technologies used

### NestJS

I really like to use this framework for Node.js. It is based on express and provides much awesome stuff, including Container Injection handling or comfortable services support. In addition, it has very good documentation and boilerplate, so I had not to spend a lot of time on configuration and I could focus on my real job.

### React

That's obvious ;)

### TypeScript

Types help us to have fewer errors and keep API bulletproof. From my point of view, TS is a better choice as well in React. It is much more advanced than Flow and other libraries.

### TypeORM

It is a Doctrine like a database library. Simply: the best of the best.

### Immer

To Immutability support. As we all know, we should avoid mutability. Immer has no own API and allows us to implement immutability in a really simple way.

### Hooks API as a state management

I always try to fit a proper tool to the problem. For this simple app, ordinary `useState`, passed in props is the best option. It is so important to keep state as close to where it is needed as possible, so Redux or Context API would be overkill there. Optionally, I would use `useReducer` instead `useState`, but it is a slightly larger cost.

### Styled Components

For a very long time, I couldn't understand why writing styles in JS is cleaner than Sass/Less, but I finally understood recently. Composition by creating styled-components is closer to React than anything else.

# Architecture

### Folder structure

In this project, I decided to create a simple folder structure. I am a big fan of splitting components and logic inside a domain (similar to DDD thinking), but in this case, I also created `scenes` and `hooks` directories to further distinguish the layers from each other.

### Encapsulation

I used many hooks to encapsulate logic. I really like to use HOC as well, but in this particular project, it wouldn't be useful.

### Functions

Pure functions are better and faster than classes, so I usually choose this kind of writing components.

### Database

I used the Data Mapper pattern to deal with a database. Thanks to the Entity model, we can perform little transforms inside them, instead of creating bad code in a different place. By the way, I played a bit creating TypeOrmFiltersBuilder with immutability support, but unfortunately `immer` doesn't support class properly yet.

# How to improve this project

I would like to left some words about improving this project. I see many things that should be implemented for good user experience and having clean code. For instance:

-   Filters should be in boxes with a reset button (something similar to input tags).
-   Search input with a reset button.
-   The sidebar should be hideable.
-   More animations could be implemented, especially slideDown/Up for filters.
-   Improve pagination working.
-   Tooltip on hover for DRG ID to show full description.
-   Database normalization. All data are stored in one table, should be split into three tables with foreign keys.
-   Interfaces and styles should be stored in different places.
-   Security staff: JWT, Auths, CSRF etc.
-   Login page, auth to API.
-   Unit tests and Storybook.

# Demo

### Frontend

[https://healthcare-frontend.skoczen.now.sh/](https://healthcare-frontend.skoczen.now.sh/)

### Backend

[https://demo-healthcare.herokuapp.com/providers](https://demo-healthcare.herokuapp.com/providers)

# Testing

I used `supertest` and `jest` combination to test the API response in the backend. These are E2E tests. To execute this test put this command to terminal:

```
$ yarn test:e2e
```
