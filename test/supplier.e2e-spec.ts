import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { INestApplication } from '@nestjs/common';
import { AppModule } from '../src/app.module';
import { response } from 'express';

describe('Providers API', () => {
    let app: INestApplication;

    const responseData: any = {
        drg_name: expect.any(String),
        name: expect.any(String),
        street: expect.any(String),
        city: expect.any(String),
        state: expect.any(String),
        zip_code: expect.any(String),
        hospital_referral_region_description: expect.any(String),
        total_discharges: expect.any(Number),
        avg_covered_charges: expect.any(String),
        avg_total_payments: expect.any(String),
        avg_medicare_payments: expect.any(String),
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        }).compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('should GET all providers by default', () => {
        return request(app.getHttpServer())
            .get('/providers')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(res => {
                expect(res.body.itemCount).toEqual(25);
                expect(res.body.next).not.toBe('');
                expect(res.body.previous).toBe('');
                expect(res.body.pageCount).toEqual(6523);
                expect(res.body.totalItems).toEqual(163065);
                expect(res.body.items).toHaveLength(25);
                expect(res.body.items[0]).toMatchObject(responseData);
            });
    });

    it('should GET providers by given name', () => {
        const value = 'ALASKA%REGIONAL%HOSPITAL';

        return request(app.getHttpServer())
            .get(`/providers?name=${value}`)
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(res => {
                expect(res.body.items[0].name).toContain(
                    'ALASKA REGIONAL HOSPITAL'
                );
            });
    });

    it('should GET providers by given state filter', () => {
        const value = 'AL';

        return request(app.getHttpServer())
            .get(`/providers?state=${value}`)
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(res => {
                expect(res.body.items[0].state).toEqual(value);
            });
    });

    it('should GET providers by given min_discharges filter', () => {
        const value = 90;

        return request(app.getHttpServer())
            .get(`/providers?min_discharges=${value}`)
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(res => {
                expect(
                    res.body.items[0].total_discharges
                ).toBeGreaterThanOrEqual(value);
            });
    });

    it('should GET providers by given max_discharges filter', () => {
        const value = 11;

        return request(app.getHttpServer())
            .get(`/providers?max_discharges=${value}`)
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(res => {
                expect(res.body.items[0].total_discharges).toBeLessThanOrEqual(
                    value
                );
            });
    });

    it('should GET providers by given both min_discharges and max_discharges filters', () => {
        const min = 11;
        const max = 20;

        return request(app.getHttpServer())
            .get(`/providers?max_discharges=${max}&min_discharges=${min}`)
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(res => {
                expect(
                    res.body.items[0].total_discharges
                ).toBeGreaterThanOrEqual(min);
                expect(res.body.items[0].total_discharges).toBeLessThanOrEqual(
                    max
                );
            });
    });

    afterAll(async () => {
        await app.close();
    });
});
